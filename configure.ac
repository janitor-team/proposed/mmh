dnl
dnl configure.ac -- autoconf template for mmh
dnl

dnl Move this up a bit
AC_PREREQ(2.61)
dnl (I was able to configure with autoconf-2.59 --meillo 2012-03-22)

AC_INIT(mmh, m4_normalize(m4_include([VERSION])))
AC_CONFIG_SRCDIR(h/mh.h)
AC_CONFIG_HEADER(config.h)

AC_CANONICAL_TARGET

dnl ---------------------
dnl define a macro or two
dnl ---------------------

AC_DEFUN([NMH_PROG_GNU_LIBTOOL], [
if test -n "$LIBTOOL" ; then
  tmptest=`$LIBTOOL --version 2>&1 | grep GNU`
  if test x"$tmptest" != x  ; then
    GNU_LIBTOOL=1
    AC_SUBST(GNU_LIBTOOL)dnl
  fi
fi
] )

echo "configuring for AC_PACKAGE_NAME-AC_PACKAGE_VERSION"
AC_SUBST(VERSION,AC_PACKAGE_VERSION)dnl

dnl What date of mmh are we building?
DATE=`cat ${srcdir}/DATE`
echo "configuring for mmh dated $DATE"
AC_SUBST(DATE)dnl

dnl --------------------------
dnl CHECK COMMAND LINE OPTIONS
dnl --------------------------

dnl Do you want to debug mmh?
AC_ARG_ENABLE(debug,
  AS_HELP_STRING([--enable-debug],[enable nmh code debugging]))
dnl The old redundant --enable-nmh-debug is deprecated and undocumented.
if test x"$enable_nmh_debug" = x"yes"; then
  enable_debug=yes
fi

dnl What method of locking to use?
AC_ARG_WITH(locking,
  AS_HELP_STRING([--with-locking=@<:@dot|fcntl|flock|lockf@:>@],
  [specify the file locking method]))

if test x"$with_locking" = x"dot"; then
  LOCKTYPE="dot"
  AC_DEFINE(DOT_LOCKING, 1, [Define to use dot based file locking.])dnl
elif test x"$with_locking" = x"flock"; then
  LOCKTYPE="flock"
  AC_DEFINE(FLOCK_LOCKING, 1, [Define to use flock() based locking.])dnl
elif test x"$with_locking" = x"lockf"; then
  LOCKTYPE="lockf"
  AC_DEFINE(LOCKF_LOCKING, 1, [Define to use lockf() based locking.])dnl
elif test x"$with_locking" = x"fcntl"; then
  LOCKTYPE="fcntl"
  AC_DEFINE(FCNTL_LOCKING, 1, [Define to use fnctl() based locking.])dnl
else
  LOCKTYPE="dot"
  AC_DEFINE(DOT_LOCKING)dnl
fi

dnl Should we use a locking directory?
AC_ARG_WITH(lockdir,
  AS_HELP_STRING([--with-lockdir=dir], [Store dot-lock files in "dir"]), [
    AS_IF([test "x$with_lockdir" = xyes],[
      AC_MSG_ERROR([--with-lockdir requires an argument])])
])

if test x"$with_lockdir" != x; then
  LOCKDIR="$with_lockdir"
  AC_DEFINE_UNQUOTED(LOCKDIR, ["$with_lockdir"],
                     [Directory to store dot-locking lock files.])dnl
fi


dnl ----------------------------------------------------
dnl Default location is /usr/local/mmh/{bin,etc,lib,share/man}
dnl ----------------------------------------------------
AC_PREFIX_DEFAULT(/usr/local/mmh)


dnl ------------------
dnl CHECK THE COMPILER
dnl ------------------
dnl We want these before the checks,
dnl so the checks can modify their values.
test -z "$CFLAGS" && CFLAGS= auto_cflags=1
if test x"$enable_debug" = x"yes"; then
  test -z "$LDFLAGS" && LDFLAGS=-g
fi

AC_PROG_CC

AC_CACHE_CHECK(whether compiler supports -Wno-pointer-sign, nmh_cv_has_noptrsign,
[nmh_saved_cflags="$CFLAGS"
 CFLAGS="$CFLAGS -Wno-pointer-sign"
 AC_TRY_COMPILE([],[],nmh_cv_has_noptrsign=yes,nmh_cv_has_noptrsign=no)
 CFLAGS="$nmh_saved_cflags"])

dnl if the user hasn't specified CFLAGS, then
dnl   if compiler is gcc, then
dnl     use -O2 and some warning flags
dnl   else use -O
dnl We use -Wall; if the compiler supports it we also use -Wno-pointer-sign,
dnl because gcc 4 now produces a lot of new warnings which are probably mostly
dnl spurious and which in any case we don't want to deal with now.
if test "$nmh_cv_has_noptrsign" = "yes"; then
  nmh_gcc_warnflags="-Wall -Wno-pointer-sign"
else
  nmh_gcc_warnflags="-Wall"
fi

if test -n "$auto_cflags"; then
  if test x"$enable_debug" = x"yes"; then
    if test -n "$GCC"; then
      test -z "$CFLAGS" && CFLAGS="$nmh_gcc_warnflags -g" || CFLAGS="$CFLAGS $nmh_gcc_warnflags -g"
    else
      test -z "$CFLAGS" && CFLAGS=-g || CFLAGS="$CFLAGS -g"
    fi
  else
    if test -z "$LDFLAGS"; then
      case "$build_os" in
        darwin*)
          LDFLAGS=
          ;;
        *)
          LDFLAGS=-s
          ;;
      esac
    fi
    if test -n "$GCC"; then
      test -z "$CFLAGS" && CFLAGS="$nmh_gcc_warnflags -O2" || CFLAGS="$CFLAGS $nmh_gcc_warnflags -O2"
    else
      test -z "$CFLAGS" && CFLAGS=-O  || CFLAGS="$CFLAGS -O"
    fi
  fi
fi

AC_C_CONST          dnl Does compiler support `const'.

dnl ------------------
dnl CHECK FOR PROGRAMS
dnl ------------------
AC_PROG_MAKE_SET    dnl Does make define $MAKE
AC_PROG_INSTALL     dnl Check for BSD compatible `install'
AC_PROG_RANLIB      dnl Check for `ranlib'
AC_PROG_AWK         dnl Check for mawk,gawk,nawk, then awk
AC_PROG_LEX         dnl Check for lex/flex

dnl Look for `cut'
pathtmp=/usr/bin:/bin:/usr/local/bin:/usr/xpg4/bin:/usr/ucb
AC_PATH_PROG(cutpath, cut, no, [$pathtmp])

dnl ----------------------------------------------
dnl check for lclint, and lint if it doesn't exist
dnl ----------------------------------------------
AC_CHECK_PROG(linttmp1, lclint, lclint, no)dnl
if test x$ac_cv_prog_linttmp1 != xno ; then
  LINT=$ac_cv_prog_linttmp1
  LINTFLAGS="-weak +posixlib -macrovarprefixexclude"
else
  AC_CHECK_PROG(linttmp2, lint, lint, no)dnl
  if test x$ac_cv_prog_linttmp2 != xno ; then
    LINT=$ac_cv_prog_linttmp2
    LINTFLAGS=""
  else
    LINT="echo 'No lint program found'"
    LINTFLAGS=""
  fi
fi
AC_SUBST(LINT)dnl
AC_SUBST(LINTFLAGS)dnl

dnl try to figure out which one we've got
AC_CHECK_PROG(LIBTOOL, libtool, libtool, , [$pathtmp])
NMH_PROG_GNU_LIBTOOL

dnl Check for lorder and tsort commands
AC_CHECK_PROG(LORDER, lorder, lorder, no)dnl
AC_CHECK_PROG(TSORT, tsort, tsort, no)dnl

dnl If either doesn't exist, replace them with echo and cat
if test x$ac_cv_prog_LORDER != xlorder -o x$ac_cv_prog_TSORT != xtsort; then
  LORDER=echo
  TSORT=cat
  AC_SUBST(LORDER)dnl
  AC_SUBST(TSORT)dnl
dnl Mac OS X has lorder, but sh is too broken for it to work
dnl elif test -z "`lorder config/config.c 2>&1 | grep config/config.c`" ; then
dnl   LORDER=echo
dnl   TSORT=cat
dnl   AC_SUBST(LORDER)dnl
dnl   AC_SUBST(TSORT)dnl
fi

dnl Check whether tsort can deal with loops
AC_CACHE_CHECK(whether tsort can deal with loops, nmh_cv_tsort_loop,
  [if test -z "`echo a b b a | tsort 2>/dev/null | grep a`" ; then
    nmh_cv_tsort_loop=no
  else
    nmh_cv_tsort_loop=yes
  fi])
if test x$nmh_cv_tsort_loop = xno ; then
  LORDER=echo
  TSORT=cat
  AC_SUBST(LORDER)dnl
  AC_SUBST(TSORT)dnl
fi

dnl Look for `ls'
pathtmp=/usr/bin:/bin:/usr/local/bin:/usr/xpg4/bin:/usr/ucb
AC_PATH_PROG(lspath, ls, no, [$pathtmp])

dnl See how we get ls to display the owner and the group
if test "$lspath" != "no"; then
  AC_CACHE_CHECK(how to get ls to show us the group ownership of a file,
                 nmh_cv_ls_grpopt,
  [if test x"`$lspath -dl / | $AWK '{print $9}'`" = x"/"; then
    dnl There were 9 parameters, so unless this is a really bizarre, nonstandard
    dnl ls, it would seem -l gave us both the user and group.  On this type of
    dnl ls, -g makes _only_ the group be displayed (and not the user).
    nmh_cv_ls_grpopt="-l"
  else
    dnl Looks like -l only gave us the user, so we need -g to get the group too.
    nmh_cv_ls_grpopt="-lg"
  fi])
fi

dnl Look for `sendmail'
pathtmp=/usr/lib:/usr/sbin:/usr/etc:/usr/ucblib:/usr/bin:/bin
AC_PATH_PROG(sendmailpath, sendmail, /usr/sbin/sendmail, [$pathtmp])

dnl Look for `vi'
pathtmp=/usr/bin:/bin:/usr/ucb:/usr/local/bin
AC_PATH_PROG(vipath, vi, /bin/vi, [$pathtmp])

dnl ----------------------------------------------------------
dnl FIND MAIL SPOOL AND SEE IF WE NEED TO MAKE inc SETGID MAIL
dnl ----------------------------------------------------------
AC_CACHE_CHECK(where mail spool is located, nmh_cv_mailspool,
[for mailspool in /var/mail        dnl
                  /var/spool/mail  dnl
                  /usr/spool/mail  dnl
                  /dev/null;       dnl Just in case we fall through
do
  test -d $mailspool && break
done
nmh_cv_mailspool=$mailspool
])
mailspool=$nmh_cv_mailspool
AC_SUBST(mailspool)dnl

dnl See whether the mail spool directory is world-writable.
if test "$lspath" != "no" -a "$cutpath" != "no"; then
  AC_CACHE_CHECK(whether the mail spool is world-writable,
                 nmh_cv_mailspool_world_writable,
  [if test "`$lspath -dl $mailspool/ | $cutpath -c9`" = "-"; then
    nmh_cv_mailspool_world_writable=no
  else
    nmh_cv_mailspool_world_writable=yes
  fi])
fi

dnl Also, check for liblockfile (as found on Debian systems)
AC_CHECK_HEADER(lockfile.h, AC_CHECK_LIB(lockfile, lockfile_create) )

dnl and whether its companion program dotlockfile is setgid
AC_PATH_PROG(dotlockfilepath, dotlockfile, no)
if test "$ac_cv_lib_lockfile_lockfile_create" != "no" ; then
  if test "$ac_cv_path_dotlockfilepath" != "no" ; then
    AC_CACHE_CHECK(whether dotlockfile is setgid, nmh_cv_dotlockfile_setgid,
    [ if test -g "$ac_cv_path_dotlockfilepath" ; then
        nmh_cv_dotlockfile_setgid=yes
      else
        nmh_cv_dotlockfile_setgid=no
      fi])
  fi
fi

dnl If mailspool is not world-writable and dotlockfile is not setgid,
dnl we need to #define MAILGROUP to 1 and make inc setgid.
if test x"$LOCKTYPE" = x"dot" -a x"$nmh_cv_mailspool_world_writable" = x"no" -a x"$nmh_cv_dotlockfile_setgid" != x"yes" ; then
  dnl do we really need both of these?
  AC_DEFINE(MAILGROUP,1,
    [Define to 1 if you need to make `inc' set-group-id because your mail spool is not world writable. There are no guarantees as to the safety of doing this, but this #define will add some extra security checks.])dnl
  SETGID_MAIL=1
fi
AC_SUBST(SETGID_MAIL)dnl

dnl Use ls to see which group owns the mail spool directory.
AC_CACHE_CHECK(what group owns the mail spool, nmh_cv_ls_mail_grp,
[nmh_cv_ls_mail_grp=`$lspath -dL $nmh_cv_ls_grpopt $mailspool|$AWK '{print $4}'`
])
MAIL_SPOOL_GRP=$nmh_cv_ls_mail_grp
AC_SUBST(MAIL_SPOOL_GRP)dnl

dnl ------------------
dnl CHECK HEADER FILES
dnl ------------------

dnl On glibc we need to define at least the '_XOPEN_SOURCE' level of features,
dnl or wchar.h doesn't declare a prototype for wcwidth(). But if we only define
dnl that level then db.h won't compile. So we define _GNU_SOURCE which turns
dnl on everything. Perhaps other OSes need some feature switch set to get wcwidth()
dnl declared; if so they should have an entry added to this case statement.
dnl NB that we must define this on the compiler command line, not in config.h,
dnl because it must be set before any system header is included and there's no
dnl portable way to make sure that files generated by lex include config.h
dnl before system header files.

case "$target_os" in
  linux*)
    # Like DEFS, but doesn't get stomped on by configure when using config.h:
    OURDEFS="$OURDEFS -D_GNU_SOURCE"
    ;;
esac
AC_SUBST(OURDEFS)

AC_HEADER_STDC
AC_HEADER_TIOCGWINSZ
AC_CHECK_HEADERS(fcntl.h crypt.h termcap.h \
                 langinfo.h wchar.h wctype.h iconv.h \
                 sys/param.h sys/time.h sys/stream.h )

dnl
dnl Checks for _IO_write_ptr. A Linuxism used by nmh on linux. We
dnl really use a whole set of them, but this check should be
dnl sufficient.
dnl
AC_CHECK_HEADER(libio.h, [
  AC_EGREP_HEADER(_IO_write_ptr, libio.h, [
    AC_DEFINE(LINUX_STDIO,1,[Use the Linux _IO_*_ptr defines from <libio.h>.]) ]) ])

AC_CHECK_HEADER([sys/ptem.h], AC_DEFINE(WINSIZE_IN_PTEM,1,
  [Define to 1 if `struct winsize' requires <sys/ptem.h>.]),,
[[#if HAVE_SYS_STREAM_H
#  include <sys/stream.h>
#endif
]])

dnl ---------------
dnl CHECK FUNCTIONS
dnl ---------------
AC_CHECK_FUNCS(nl_langinfo mbtowc wcwidth)

dnl Look for the initgroups() declaration.  On AIX 4.[13], Solaris 4.1.3, and
dnl ULTRIX 4.2A the function is defined in libc but there's no declaration in
dnl any system header.
dnl
dnl On Solaris 2.[456], the declaration is in <grp.h>.  On HP-UX 9-11 and
dnl (reportedly) FreeBSD 3.[23], it's in <unistd.h>.  Any other locations we
dnl need to check?
AH_TEMPLATE(INITGROUPS_HEADER, [Define to the header containing the declaration of `initgroups'.])
AC_EGREP_HEADER(initgroups, grp.h, AC_DEFINE(INITGROUPS_HEADER, <grp.h>),
                AC_EGREP_HEADER(initgroups, unistd.h,
                                AC_DEFINE(INITGROUPS_HEADER, <unistd.h>)))

dnl Check for multibyte character set support
if test "x$ac_cv_header_wchar_h" = "xyes" -a "x$ac_cv_header_wctype_h" = "xyes" \
    -a "x$ac_cv_func_wcwidth" = "xyes" -a "x$ac_cv_func_mbtowc" = "xyes"; then
  AC_DEFINE(MULTIBYTE_SUPPORT, 1,
    [Define to enable support for multibyte character sets.])
fi

dnl -------------------
dnl CHECK FOR LIBRARIES
dnl -------------------
dnl Check location of modf
AC_CHECK_FUNC(modf, , AC_CHECK_LIB(m, modf))

termcap_curses_order="termcap curses ncurses tinfo"
for lib in $termcap_curses_order; do
  AC_CHECK_LIB(${lib}, tgetent, [TERMLIB="-l$lib"; break])
done
AC_SUBST(TERMLIB)dnl
if test "x$TERMLIB" = "x"; then
	echo 'Could not find tgetent() in any library.'
	echo 'Is there a ncurses-devel package that you can install?'
	exit 1
fi


dnl ---------------
dnl CHECK FOR ICONV
dnl ---------------

dnl Find iconv. It may be in libiconv and may be iconv() or libiconv()
if test "x$ac_cv_header_iconv_h" = "xyes"; then
  AC_CHECK_FUNC(iconv, ac_found_iconv=yes, ac_found_iconv=no)
  if test "x$ac_found_iconv" = "xno"; then
    AC_CHECK_LIB(iconv, iconv, ac_found_iconv=yes)
    if test "x$ac_found_iconv" = "xno"; then
      AC_CHECK_LIB(iconv, libiconv, ac_found_iconv=yes)
    fi
    if test "x$ac_found_iconv" != "xno"; then
      LIBS="-liconv $LIBS"
    fi
  else
    dnl Handle case where there is a native iconv but iconv.h is from libiconv
    AC_CHECK_DECL(_libiconv_version,
      [ AC_CHECK_LIB(iconv, libiconv, LIBS="-liconv $LIBS") ],,
      [ #include <iconv.h> ])
  fi
fi
if test "x$ac_found_iconv" = xyes; then
  AC_DEFINE(HAVE_ICONV, 1, [Define if you have the iconv() function.])
fi

dnl Check if iconv uses const in prototype declaration
if test "x$ac_found_iconv" = "xyes"; then
  AC_CACHE_CHECK(for iconv declaration, ac_cv_iconv_const,
    [AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include <stdlib.h>
        #include <iconv.h>]],
        [[#ifdef __cplusplus
          "C"
          #endif
          #if defined(__STDC__) || defined(__cplusplus)
          size_t iconv (iconv_t cd, char * *inbuf, size_t *inbytesleft, char * *outbuf, size_t *outbytesleft);
          #else
          size_t iconv();
          #endif]])],
      [ac_cv_iconv_const=],
      [ac_cv_iconv_const=const])])
  AC_DEFINE_UNQUOTED([ICONV_CONST], $ac_cv_iconv_const,
    [Define as const if the declaration of iconv() needs const.])
fi


dnl ---------------------
dnl CHECK TERMCAP LIBRARY
dnl ---------------------

dnl Add the termcap library, so that the following configure
dnl tests will find it when it tries to link test programs.
nmh_save_LIBS="$LIBS"
LIBS="$TERMLIB $LIBS"

dnl Check if tgetent accepts NULL (and will allocate its own termcap buffer)
dnl Some termcaps reportedly accept a zero buffer, but then dump core
dnl in tgetstr().
dnl Under Cygwin test program crashes but exit code is still 0. So,
dnl we test for a file that porgram should create
AH_TEMPLATE([TGETENT_ACCEPTS_NULL],
[Define to 1 if tgetent() accepts NULL as a buffer.])
AC_CACHE_CHECK(if tgetent accepts NULL,
nmh_cv_func_tgetent_accepts_null,
[AC_TRY_RUN([
main()
{
	char buf[4096];
	int r1 = tgetent(buf, "vt100");
	int r2 = tgetent(NULL,"vt100");
	if (r1 >= 0 && r1 == r2) {
		char tbuf[1024], *u;
		u = tbuf;
			tgetstr("cl", &u);
		creat("conftest.tgetent", 0640);
	}
	exit((r1 != r2) || r2 == -1);
}
],
  if test -f conftest.tgetent; then
    nmh_cv_func_tgetent_accepts_null=yes
  else
    nmh_cv_func_tgetent_accepts_null=no
  fi,
  nmh_cv_func_tgetent_accepts_null=no,
  nmh_cv_func_tgetent_accepts_null=no)])
if test x$nmh_cv_func_tgetent_accepts_null = xyes; then
  AC_DEFINE(TGETENT_ACCEPTS_NULL)
fi
AC_CACHE_CHECK(if tgetent returns 0 on success,
nmh_cv_func_tgetent_zero_success,
[AC_TRY_RUN([
main()
{
	char buf[4096];
	int r1 = tgetent(buf, "!@#$%^&*");
	int r2 = tgetent(buf, "vt100");
	if (r1 < 0 && r2 == 0) {
		char tbuf[1024], *u;
		u = tbuf;
			tgetstr("cl", &u);
		creat("conftest.tgetent0", 0640);
	}
	exit(r1 == r2);
}
],
  if test -f conftest.tgetent0; then
    nmh_cv_func_tgetent_zero_success=yes
  else
    nmh_cv_func_tgetent_zero_success=no
  fi,
  nmh_cv_func_tgetent_zero_success=no,
  nmh_cv_func_tgetent_zero_success=no)])
AH_TEMPLATE([TGETENT_SUCCESS],
[Define to what tgetent() returns on success (0 on HP-UX X/Open curses).])
if test x$nmh_cv_func_tgetent_zero_success = xyes; then
  AC_DEFINE(TGETENT_SUCCESS, 0)
else
  AC_DEFINE(TGETENT_SUCCESS, 1)
fi

dnl Now put the libraries back to what it was before we
dnl starting checking the termcap library.
LIBS="$nmh_save_LIBS"

dnl --------------
dnl CHECK TYPEDEFS
dnl --------------
AC_TYPE_PID_T
AC_TYPE_OFF_T
AC_TYPE_UID_T
AC_TYPE_MODE_T
AC_TYPE_SIZE_T

dnl ----------------
dnl CHECK STRUCTURES
dnl ----------------

dnl For platforms such as FreeBSD that have tm_gmtoff in struct tm.
dnl (FreeBSD has a timezone() function but not a timezone global
dnl variable that is visible).
AC_CHECK_MEMBERS([struct tm.tm_gmtoff],,,[#include <time.h>])

AC_STRUCT_DIRENT_D_TYPE

dnl Where is <signal.h> located?  Needed as input for signames.awk
AC_CACHE_CHECK(where signal.h is located, nmh_cv_path_signal_h,
[for SIGNAL_H in /usr/include/bsd/sys/signal.h  dnl Next
                 /usr/include/asm/signal.h      dnl Linux 1.3.0 and above
                 /usr/include/asm/signum.h      dnl some versions of Linux/Alpha
                 /usr/include/linux/signal.h    dnl Linux up to 1.2.11
                 /usr/include/sys/signal.h      dnl Almost everybody else
                 /dev/null;                     dnl Just in case we fall through
do
  test -f $SIGNAL_H && \
  grep '#[         ]*define[         ][         ]*SIG[0-9A-Z]*[         ]*[0-9][0-9]*' $SIGNAL_H > /dev/null && \
  break
done
nmh_cv_path_signal_h=$SIGNAL_H
])
SIGNAL_H=$nmh_cv_path_signal_h
AC_SUBST(SIGNAL_H)dnl


dnl ----------------
dnl OUTPUT MAKEFILES
dnl ----------------
AC_CONFIG_FILES(Makefile config/Makefile h/Makefile sbr/Makefile uip/Makefile \
                etc/Makefile man/Makefile)
AC_CONFIG_COMMANDS([stamp],[test -z "$CONFIG_HEADERS" || echo > stamp-h])
AC_OUTPUT

dnl These odd looking assignments are done to expand out unexpanded
dnl variables in bindir et al (for instance mandir is '${datarootdir}/man',
dnl but expanding that gives '${prefix}/share/man', so we need to expand
dnl again to get the final answer.
dnl We only use the expanded versions to print the install paths in
dnl the final summary and should use them nowhere else (see the autoconf
dnl docs for the rationale for bindir etc being unexpanded).
eval "nmhbin=${bindir}";         eval "nmhbin=${nmhbin}"
eval "nmhsysconf=${sysconfdir}"; eval "nmhsysconf=${nmhsysconf}"
eval "nmhlib=${libdir}";         eval "nmhlib=${nmhlib}"
eval "nmhman=${mandir}";         eval "nmhman=${nmhman}"

cat <<!
mmh configuration
-----------------
mmh version            : AC_PACKAGE_VERSION
target os              : ${target}
compiler               : ${CC}
compiler flags         : ${CFLAGS}
linker flags           : ${LDFLAGS}
definitions            : ${OURDEFS}
source code location   : ${srcdir}

binary install path    : ${nmhbin}
library install path   : ${nmhlib}
config install path    : ${nmhsysconf}
man page install path  : ${nmhman}

sendmail path          : ${sendmailpath}
mail spool             : ${mailspool}

file locking type      : ${LOCKTYPE}
!
if test x"$LOCKTYPE" = x"dot" && test x"$LOCKDIR" != x; then
	echo "lockfile directory     : ${LOCKDIR}"
fi
