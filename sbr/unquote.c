/*
** unquote.c: Handle quote removal and quoted-pair strings on
** RFC 2822-5322 atoms.
**
** This code is Copyright (c) 2013, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>

/*
** Remove quotes and quoted-pair sequences from RFC-5322 atoms.
**
** Currently the actual algorithm is simpler than it technically should
** be: any quotes are simply eaten, unless they're preceded by the escape
** character (\).  This seems to be sufficient for our needs for now.
**
** Arguments:
**
** input      - The input string
** output     - The output string; is assumed to have at least as much
**              room as the input string.  At worst the output string will
**              be the same size as the input string; it might be smaller.
*/
void
unquote_string(const char *input, char *output)
{
	int inpos = 0;
	int outpos = 0;

	while (input[inpos] != '\0') {
		switch (input[inpos]) {
		case '\\':
			inpos++;
			if (input[inpos] != '\0')
				output[outpos++] = input[inpos++];
			break;
		case '"':
			inpos++;
			break;
		default:
			output[outpos++] = input[inpos++];
			break;
		}
	}
	output[outpos] = '\0';
}
