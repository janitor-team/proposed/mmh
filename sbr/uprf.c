/*
** uprf.c -- "unsigned" lexical prefix
**        -- Check if `word' starts with `prefix', caseinsensitively.
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>


int
uprf(char *word, char *prefix)
{
	if (!word || !prefix) {
		return 0;
	}
	return (strncasecmp(word, prefix, strlen(prefix))==0) ? 1 : 0;
}
