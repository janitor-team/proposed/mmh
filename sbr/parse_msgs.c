#include <h/mh.h>
#include <h/utils.h>

int
parse_msgs(const struct msgs_array *msgs, char *folder, struct msgs_array *files)
{
	int ret = 0;
	char *msgnam;
	struct msgs *f;
	char *path;
	size_t i;

	if (folder) {
		path = toabsdir(folder);
	} else {
		path = toabsdir(getcurfol());
	}

	path = mh_xstrdup(path);
	path = add("/", path);

	f = folder_read(path);

	if (f == NULL) {
		return -1;
	}

	for (i = 0; i < msgs->size; i++) {
		if (*(msgs->msgs[i]) == '.' || *(msgs->msgs[i]) == '/') {
				app_msgarg(files, msgs->msgs[i]);
				continue;
		}

		if (!m_convert(f, msgs->msgs[i])) {
			ret++;
		}
	}

	for (i = f->lowsel; f->numsel > 0 && i <= f->hghsel; i++) {
		if (is_selected(f, i)) {
			msgnam = mh_xstrdup(path);
			msgnam = add(m_name(i), msgnam);
			app_msgarg(files, msgnam);
		}
	}

	folder_free(f);

	return ret;
}
