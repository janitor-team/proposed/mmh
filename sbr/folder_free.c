/*
** folder_free.c -- free a folder/message structure
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>

void
folder_free(struct msgs *mp)
{
	int i;

	if (!mp)
		return;

	if (mp->foldpath)
		mh_free0(&(mp->foldpath));

	/* free the sequence names */
	for (i = 0; mp->msgattrs[i]; i++)
		mh_free0(&(mp->msgattrs[i]));

	mh_free0(&(mp->msgstats));  /* free message status area */
	mh_free0(&mp);  /* free main folder structure */
}
