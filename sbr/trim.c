/*
** trim.c -- strip leading and trailing whitespace ... inplace!
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <ctype.h>


char *
trim(unsigned char *cp)
{
	unsigned char *sp;

	/* skip over leading whitespace */
	while (isspace(*cp)) {
		cp++;
	}

	/* start at the end and zap trailing whitespace */
	for (sp = cp + strlen(cp) - 1; sp >= cp; sp--) {
		if (isspace(*sp)) {
			*sp = '\0';
		} else {
			break;
		}
	}

	return cp;
}

char *
rtrim(char *cp)
{
	char *sp = cp + strlen(cp) - 1;

	while (sp >= cp && isspace(*sp)) {
		sp--;
	}
	*++sp = '\0';
	return cp;
}
