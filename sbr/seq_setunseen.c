/*
** seq_setunseen.c -- add/delete all messages which have the SELECT_UNSEEN
**                 -- bit set to/from the Unseen-Sequence
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>

/*
** We scan through the folder and act upon all messages
** that are marked with the SELECT_UNSEEN bit.
**
** Either add messages to or (if doadd is false) delete messages from
** the unseen sequence(s).
*/
void
seq_setunseen(struct msgs *mp, int doadd)
{
	int n;
	char **ap, *cp, *dp;

	if (mp->lowmsg == 0) {
		return;
	}

	/*
	** Get the list of sequences for Unseen-Sequence
	** and split them.
	*/
	if ((cp = context_find(usequence))) {
		dp = mh_xstrdup(cp);
	} else {
		/* not set in profile, thus use the default */
		dp = mh_xstrdup(seq_unseen);
	}
	if (!(ap = brkstring(dp, " ", "\n")) || !*ap) {
		/* contains no sequence name, i.e. we're finished */
		mh_free0(&dp);
		return;
	}

	/*
	** Now add/delete each message which has the SELECT_UNSEEN
	** bit set to/from each of these sequences.
	*/
	for (; *ap; ap++) {
		if (doadd) {
			for (n = mp->lowmsg; n <= mp->hghmsg; n++) {
				if (is_unseen(mp, n)) {
					seq_addmsg(mp, *ap, n, -1, 0);
				}
			}
		} else {
			/* make sure sequence exists first */
			if (seq_getnum(mp, *ap) != -1) {
				for (n = mp->lowsel; n <= mp->hghsel; n++) {
					if (is_unseen(mp, n)) {
						seq_delmsg(mp, *ap, n);
					}
				}
			}
		}
	}

	mh_free0(&dp);
}
