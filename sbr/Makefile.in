#
# Makefile for sbr subdirectory
#

SHELL = /bin/sh

top_srcdir = @top_srcdir@
srcdir     = @srcdir@
VPATH      = @srcdir@

VERSION = `$(top_srcdir)/version.sh $(top_srcdir)`

prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
etcdir      = @sysconfdir@

CC       = @CC@
CFLAGS   = @CFLAGS@
DEFS     = @DEFS@
# add include dir . for sigmsg.h and .. for config.h when building
# in different directory
INCLUDES = -I$(top_srcdir) -I. -I.. @CPPFLAGS@

LEX    = @LEX@
AWK    = @AWK@
LORDER = @LORDER@
TSORT  = @TSORT@
RANLIB = @RANLIB@
LIBTOOL = @LIBTOOL@
GNU_LIBTOOL = @GNU_LIBTOOL@
LINT    = @LINT@
LINTFLAGS = @LINTFLAGS@

COMPILE = $(CC) -c $(DEFS) $(INCLUDES) $(CFLAGS)

LOCALLIBS = ../config/config.o

.SUFFIXES:
.SUFFIXES: .c .o

.c.o:
	$(COMPILE) $<

# this header file is parsed to generate signal messages (sigmsg.h)
SIGNAL_H = @SIGNAL_H@

# source for library functions
SRCS = addrsbr.c ambigsw.c brkstring.c  \
	charset.c concat.c context_del.c  \
	context_find.c context_read.c  \
	context_replace.c context_save.c \
	cpydata.c crawl_folders.c  \
	dtime.c dtimep.c  \
	error.c execprog.c ext_hook.c folder_addmsg.c folder_delmsgs.c  \
	folder_free.c folder_read.c  \
	folder_realloc.c gans.c getans.c getanswer.c  \
	getarguments.c \
	fmt_addr.c fmt_compile.c fmt_new.c fmt_rfc2047.c  \
	fmt_scan.c lock_file.c m_atoi.c \
	m_convert.c m_draft.c m_getfld2.c m_gmprot.c  \
	m_name.c \
	makedir.c mts.c norm_charmap.c  \
	path.c pidwait.c pidstatus.c  \
	print_help.c print_sw.c print_version.c \
	putenv.c mhbasename.c  \
	readconfig.c seq_add.c seq_bits.c  \
	seq_del.c seq_getnum.c seq_list.c seq_nameok.c  \
	seq_print.c seq_read.c seq_save.c seq_setcur.c  \
	seq_setprev.c seq_setunseen.c signals.c  \
	smatch.c snprintb.c strcasecmp.c  \
	strindex.c trim.c trimcpy.c uprf.c vfgets.c fmt_def.c  \
	mf.c utils.c m_mktemp.c seq_msgstats.c \
	unquote.c encode_rfc2047.c parse_msgs.c \
	getthreadid.c

OBJS =  $(SRCS:.c=.o)

# ========= DEPENDENCIES FOR BUILDING ==========

# default target
all: libmh.a

sigmsg.h: sigmsg.awk
	$(AWK) -f $(srcdir)/sigmsg.awk $(SIGNAL_H) > $@

lint: sigmsg.h
	$(LINT) $(LINTFLAGS) $(INCLUDES) $(DEFS) $(SRCS)

# Note that not all lexes support -o (it is not POSIX); also
# some lexes will only accept '-n -t', not '-nt'.
# Also, not all makes accept $< in non-pattern rules,
# hence the explicit filenames here.
dtimep.c: dtimep.lex
	$(LEX) -n -t $(srcdir)/dtimep.lex > $@

pidstatus.o: sigmsg.h

libmh.a: $(OBJS) $(LOCALLIBS) version.c
	rm -f $@
	$(COMPILE) -DVERSION="\"$(VERSION)\"" -o version.o $(srcdir)/version.c
	if test x$(LIBTOOL) != x -a x$(GNU_LIBTOOL) = x ; then \
	  $(LIBTOOL) -static -c -o libmh.a $(OBJS) $(LOCALLIBS) version.o; \
	else \
	  ar cr libmh.a `$(LORDER) $(OBJS) $(LOCALLIBS) version.o | $(TSORT) 2>/dev/null`  ; \
	  $(RANLIB) libmh.a  ; \
	fi
	rm -f version.o

install:

uninstall:

# ========== DEPENDENCIES FOR CLEANUP ==========

mostlyclean:
	rm -f *.o *~

clean: mostlyclean
	rm -f libmh.a sigmsg.h

distclean: clean
	rm -f Makefile dtimep.c

realclean: distclean

superclean: realclean

# ========== DEPENDENCIES FOR MAINTENANCE ==========

subdir = sbr

Makefile: Makefile.in ../config.status
	cd .. && ./config.status $(subdir)/$@
