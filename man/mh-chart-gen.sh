#! /bin/sh
#
# Generates mh-chart man page from other man page source files that have a
# SYNOPSIS section.

cat <<!
.\"
.\" %nmhwarning%
.\"
.TH MH-CHART %manext7% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
mh-chart \- Chart of mmh Commands
.SH SYNOPSIS
.na
!

for i do
	#### Extract lines from just after .SH SYNOPSIS to just before .ad.
	<"$i" awk '
		/.SH SYNOPSIS/ {p=1}
		/^\.ad/ {p=0}
		/^(\.SH SYNOPSIS|\.na|\.ad)/ {next}
		p
	'
	echo
done

cat <<!
.ad

.SH "SEE ALSO"
mmh(1)
!
