.\"
.\" %nmhwarning%
.\"
.TH ALI %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
ali \- list mail aliases
.SH SYNOPSIS
.HP 5
.na
.B ali
.RB [ \-file
.IR aliasfile ]...
.RB [ \-list " | " \-nolist ]
.RB [ \-normalize " | " \-nonormalize ]
.RB [ \-user " | " \-nouser ]
.RB [ \-Version ]
.RB [ \-help ]
.RI [ aliases " ...]"
.ad
.SH DESCRIPTION
.B Ali
searches the named mail alias files for each of the given
.IR aliases .
It creates a list of addresses for those
.IR aliases ,
and writes that list on standard output.  If no arguments are given,
.B ali
outputs all alias entries.
This can be used to check the format of the alias file for validity.
.PP
By default, when an alias expands to multiple addresses, the addresses
are separated by commas and printed on as few lines as possible.  If the
.B \-list
switch is specified, and an address expands to multiple
addresses, each address will appear on a separate line.
.PP
The
.B \-user
switch directs
.B ali
to perform its processing in
an inverted fashion: instead of listing the addresses that each given
alias expands to,
.B ali
will list the aliases that expand to each
given address.  If the
.B \-normalize
switch is given,
.B ali
will
try to track down the official hostname of the address.
.PP
If no
.B \-file
.I aliasfile
is given, then the default alias files, as specified by the profile entry
.RI ` Aliasfile ',
will be read.
If any alias files are given by
.B \-file
.I aliasfile
switches, these will be read instead of the default alias files.
Either the default alias files are read or the ones given at the
command line, never both.
.PP
Each
.I alias
is processed as described in
.BR mh\-alias (5).
.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^$HOME/.mmh/profile~^The user profile
^/etc/passwd~^List of users
^/etc/group~^List of groups
.fi
.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Aliasfile:~^For default alias files
.fi
.SH "SEE ALSO"
mh\-alias(5)
.SH DEFAULTS
.nf
.RB ` \-nolist '
.RB ` \-nonormalize '
.RB ` \-nouser '
.fi
.SH CONTEXT
None
.SH BUGS
The
.B \-user
option with
.B \-nonormalize
is not entirely accurate, as it
does not replace local nicknames for hosts with their official site names.
