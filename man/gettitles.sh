#!/bin/sh

cd $1 || exit 1
shift

for i in *.man"$1" ; do
	sed -n '
		/^.SH NAME/{
			n
			s/^/^/
			s/\([^ ]*\) \\-/\1('"$1"')~^\\-/
			s/\([^ ]*\), \([^ ]*\)/\1('"$1"'),\n.br\n^    \2/
			p
			q
		}
	' "$i"
done
