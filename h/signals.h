/*
** signals.h -- header file for nmh signal interface
*/

#include <config.h>
#include <signal.h>

/*
** The type for a signal handler
*/
typedef void (*SIGNAL_HANDLER)(int);

/*
** prototypes
*/
SIGNAL_HANDLER SIGNAL(int, SIGNAL_HANDLER);
SIGNAL_HANDLER SIGNAL2(int, SIGNAL_HANDLER);
