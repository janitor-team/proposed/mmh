/*
** dropsbr.h -- definitions for maildrop-style files
*/

/*
** prototypes
*/
int mbox_open(char *, uid_t, gid_t, mode_t);
int mbox_copy(int, int);
int mbox_close(char *, int);
