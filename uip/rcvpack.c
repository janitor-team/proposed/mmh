/*
** rcvpack.c -- append message to a file
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/dropsbr.h>
#include <h/rcvmail.h>
#include <h/tws.h>
#include <unistd.h>
#include <locale.h>
#include <sysexits.h>

static struct swit switches[] = {
#define VERSIONSW  0
	{ "Version", 0 },
#define HELPSW  1
	{ "help", 0 },
	{ NULL, 0 }
};

char *version=VERSION;

int
main(int argc, char **argv)
{
	int md;
	char *cp, *file = NULL, buf[BUFSIZ];
	char **argp, **arguments;

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	/* parse arguments */
	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf),
						"%s [switches] file",
						invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			}
		}
		if (file)
			adios(EX_USAGE, NULL, "only one file at a time!");
		else
			file = cp;
	}

	/* copy stdin to stdout, converting rfc822 message to mbox */
	if (!file) {
		if (mbox_copy(fileno(stdout), fileno(stdin)) == NOTOK) {
			exit(RCV_MBX);
		}
		exit(RCV_MOK);
		return 1;
	}

	/* open and lock the file */
	if ((md = mbox_open(file, getuid(), getgid(), m_gmprot()))
			== NOTOK)
		exit(RCV_MBX);

	/* append the message */
	if (mbox_copy(md, fileno(stdin)) == NOTOK) {
		mbox_close(file, md);
		exit(RCV_MBX);
	}

	/* close and unlock the file */
	if (mbox_close(file, md) == NOTOK)
		exit(RCV_MBX);

	exit(RCV_MOK);
	return 1;
}
