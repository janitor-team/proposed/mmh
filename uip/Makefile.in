#
# Makefile for uip subdirectory
#

SHELL = /bin/sh

top_srcdir = @top_srcdir@
srcdir     = @srcdir@
VPATH      = @srcdir@

VERSION = `$(top_srcdir)/version.sh $(top_srcdir)`

prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
etcdir      = @sysconfdir@

CC       = @CC@
CFLAGS   = @CFLAGS@
DEFS     = @DEFS@
# add include dir .. for config.h when building in different directory
INCLUDES = -I$(srcdir) -I$(top_srcdir) -I.. @CPPFLAGS@
LDFLAGS  = @LDFLAGS@

LIBS     = @LIBS@
LOCALLIBS = ../sbr/libmh.a
LINKLIBS = $(LOCALLIBS) $(LIBS)

LINT    = @LINT@
LINTFLAGS = @LINTFLAGS@

TERMLIB = @TERMLIB@
LEXLIB  = @LEXLIB@

COMPILE = $(CC) -c $(DEFS) -DVERSION="\"$(VERSION)\"" $(INCLUDES) $(CFLAGS)
LINK    = $(CC) $(LDFLAGS) -o $@
LN = ln

INSTALL         = @INSTALL@
INSTALL_PROGRAM = @INSTALL_PROGRAM@

MAIL_SPOOL_GRP = @MAIL_SPOOL_GRP@
SETGID_MAIL    = @SETGID_MAIL@

.SUFFIXES:
.SUFFIXES: .c .o

.c.o:
	$(COMPILE) $<

# commands to build
CMDS = ali anno burst comp dist flist folder forw mmh mark \
       mhbuild mhl mhsign mhpgp \
       mhlist mhmail mhparam mhpath mhstore new packf pick \
       print-mimetype prompter rcvdist rcvpack rcvstore refile repl rmf \
       rmm send sendfiles show slocal sortm spost whatnow whatnow2 whom

# commands that are links to other commands
LCMDS = flists folders next prev fnext fprev unseen scan

# misc support binaries
MISC = ap dp fmtdump mhtest mmhwrap

# commands with 'S'pecial installation needs
SCMDS = inc

# source files
SRCS = ali.c aliasbr.c anno.c ap.c burst.c comp.c \
	dist.c distsbr.c dp.c dropsbr.c flist.c fmtdump.c \
	folder.c forw.c inc.c mark.c mmh.sh mmhwrap.sh mhbuild.c \
	mhfree.c mhl.c mhlist.c mhlistsbr.c mhsign.sh mhpgp.sh \
	mhmail.c mhmisc.c mhoutsbr.c mhparam.c mhparse.c \
	mhpath.c mhshow.c mhshowsbr.c mhstore.c mhtest.c \
	new.c packf.c pick.c print-mimetype.sh \
	prompter.c rcvdist.c rcvpack.c rcvstore.c \
	refile.c repl.c rmf.c rmm.c scansbr.c send.c \
	sendfiles.sh slocal.c sortm.c spost.c termsbr.c \
	whatnow.c whatnowproc.c whom.c whatnow2.sh

# ========== DEFAULT TARGET ==========

all: $(CMDS) $(MISC) $(SCMDS)

# ========= DEPENDENCIES FOR BUILDING ==========

ali: ali.o aliasbr.o $(LOCALLIBS)
	$(LINK) ali.o aliasbr.o $(LINKLIBS)

ap: ap.o $(LOCALLIBS)
	$(LINK) ap.o $(LINKLIBS)

anno: anno.o $(LOCALLIBS)
	$(LINK) anno.o $(LINKLIBS)

burst: burst.o $(LOCALLIBS)
	$(LINK) burst.o $(LINKLIBS)

comp: comp.o whatnowproc.o $(LOCALLIBS)
	$(LINK) comp.o whatnowproc.o $(LINKLIBS)

dist: dist.o whatnowproc.o $(LOCALLIBS)
	$(LINK) dist.o whatnowproc.o $(LINKLIBS)

dp: dp.o $(LOCALLIBS)
	$(LINK) dp.o $(LINKLIBS)

flist: flist.o $(LOCALLIBS)
	$(LINK) flist.o $(LINKLIBS)

fmtdump: fmtdump.o $(LOCALLIBS)
	$(LINK) fmtdump.o $(LINKLIBS)

folder: folder.o $(LOCALLIBS)
	$(LINK) folder.o $(LINKLIBS)

forw: forw.o whatnowproc.o $(LOCALLIBS)
	$(LINK) forw.o whatnowproc.o $(LINKLIBS)

inc: inc.o scansbr.o termsbr.o $(LOCALLIBS)
	$(LINK) inc.o scansbr.o termsbr.o $(LINKLIBS) $(TERMLIB)

mark: mark.o $(LOCALLIBS)
	$(LINK) mark.o $(LINKLIBS)

mmh: mmh.sh
	cp $(srcdir)/mmh.sh mmh
	chmod +x mmh

mmhwrap: mmhwrap.sh
	sed "s,%bindir%,"$(bindir)"," $(srcdir)/mmhwrap.sh >mmhwrap
	chmod +x mmhwrap

mhsign: mhsign.sh
	sed "s,%libdir%,"$(libdir)"," $(srcdir)/mhsign.sh >mhsign
	chmod +x mhsign

mhpgp: mhpgp.sh
	cp $(srcdir)/mhpgp.sh mhpgp
	chmod +x mhpgp

mhbuild: mhbuild.o mhoutsbr.o mhmisc.o mhfree.o mhparse.o termsbr.o $(LOCALLIBS)
	$(LINK) mhbuild.o mhoutsbr.o mhmisc.o mhfree.o mhparse.o $(LINKLIBS) $(TERMLIB)

mhl: mhl.o termsbr.o $(LOCALLIBS)
	$(LINK) mhl.o termsbr.o $(LINKLIBS) $(TERMLIB)

mhlist: mhlist.o mhparse.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LOCALLIBS)
	$(LINK) mhlist.o mhparse.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LINKLIBS) $(TERMLIB)

mhmail: mhmail.o $(LOCALLIBS)
	$(LINK) mhmail.o $(LINKLIBS)

mhparam: mhparam.o $(LOCALLIBS)
	$(LINK) mhparam.o $(LINKLIBS)

mhpath: mhpath.o $(LOCALLIBS)
	$(LINK) mhpath.o $(LINKLIBS)

show: mhshow.o mhparse.o mhshowsbr.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LOCALLIBS)
	$(LINK) mhshow.o mhparse.o mhshowsbr.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LINKLIBS) $(TERMLIB)

mhstore: mhstore.o mhparse.o mhshowsbr.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LOCALLIBS)
	$(LINK) mhstore.o mhparse.o mhshowsbr.o mhlistsbr.o mhmisc.o mhfree.o termsbr.o $(LINKLIBS) $(TERMLIB)

mhtest: mhtest.o mhparse.o mhoutsbr.o mhmisc.o mhfree.o termsbr.o $(LOCALLIBS)
	$(LINK) mhtest.o mhparse.o mhoutsbr.o mhmisc.o mhfree.o termsbr.o $(LINKLIBS) $(TERMLIB)

new: new.o $(LOCALLIBS)
	$(LINK) new.o $(LINKLIBS)

packf: packf.o dropsbr.o $(LOCALLIBS)
	$(LINK) packf.o dropsbr.o $(LINKLIBS)

pick: pick.o scansbr.o termsbr.o $(LOCALLIBS)
	$(LINK) pick.o scansbr.o termsbr.o $(LINKLIBS) $(TERMLIB)

print-mimetype: print-mimetype.sh
	cp $(srcdir)/print-mimetype.sh print-mimetype
	chmod +x print-mimetype

prompter: prompter.o $(LOCALLIBS)
	$(LINK) prompter.o $(LINKLIBS)

rcvdist: rcvdist.o distsbr.o $(LOCALLIBS)
	$(LINK) rcvdist.o distsbr.o $(LINKLIBS)

rcvpack: rcvpack.o dropsbr.o $(LOCALLIBS)
	$(LINK) rcvpack.o dropsbr.o $(LINKLIBS)

rcvstore: rcvstore.o $(LOCALLIBS)
	$(LINK) rcvstore.o $(LINKLIBS)

refile: refile.o $(LOCALLIBS)
	$(LINK) refile.o $(LINKLIBS)

repl: repl.o whatnowproc.o $(LOCALLIBS)
	$(LINK) repl.o whatnowproc.o $(LINKLIBS)

rmf: rmf.o $(LOCALLIBS)
	$(LINK) rmf.o $(LINKLIBS)

rmm: rmm.o $(LOCALLIBS)
	$(LINK) rmm.o $(LINKLIBS)

send: send.o distsbr.o $(LOCALLIBS)
	$(LINK) send.o distsbr.o $(LINKLIBS)

sendfiles: sendfiles.sh
	cp $(srcdir)/sendfiles.sh sendfiles
	chmod +x sendfiles

slocal: slocal.o $(LOCALLIBS)
	$(LINK) slocal.o $(LINKLIBS)

sortm: sortm.o $(LOCALLIBS)
	$(LINK) sortm.o $(LINKLIBS)

spost: spost.o aliasbr.o $(LOCALLIBS)
	$(LINK) spost.o aliasbr.o $(LINKLIBS)

whatnow: whatnow.o $(LOCALLIBS)
	$(LINK) whatnow.o $(LINKLIBS)

whatnow2: whatnow2.sh
	cp $(srcdir)/whatnow2.sh whatnow2

whom: whom.o $(LOCALLIBS)
	$(LINK) whom.o $(LINKLIBS)

# ========== DEPENDENCIES FOR INSTALLING ==========

# install everything
install: install-cmds install-misc install-lcmds install-scmds

# install commands
install-cmds:
	mkdir -p $(DESTDIR)$(bindir)
	for cmd in $(CMDS); do \
	  $(INSTALL_PROGRAM) $$cmd $(DESTDIR)$(bindir)/$$cmd; \
	done

# install links
install-lcmds: install-cmds
	rm -f $(DESTDIR)$(bindir)/flists
	rm -f $(DESTDIR)$(bindir)/folders
	rm -f $(DESTDIR)$(bindir)/fnext
	rm -f $(DESTDIR)$(bindir)/fprev
	rm -f $(DESTDIR)$(bindir)/unseen
	rm -f $(DESTDIR)$(bindir)/prev
	rm -f $(DESTDIR)$(bindir)/next
	rm -f $(DESTDIR)$(bindir)/scan
	$(LN) $(DESTDIR)$(bindir)/flist  $(DESTDIR)$(bindir)/flists
	$(LN) $(DESTDIR)$(bindir)/folder $(DESTDIR)$(bindir)/folders
	$(LN) $(DESTDIR)$(bindir)/new    $(DESTDIR)$(bindir)/fnext
	$(LN) $(DESTDIR)$(bindir)/new    $(DESTDIR)$(bindir)/fprev
	$(LN) $(DESTDIR)$(bindir)/new    $(DESTDIR)$(bindir)/unseen
	$(LN) $(DESTDIR)$(bindir)/show   $(DESTDIR)$(bindir)/prev
	$(LN) $(DESTDIR)$(bindir)/show   $(DESTDIR)$(bindir)/next
	$(LN) $(DESTDIR)$(bindir)/pick   $(DESTDIR)$(bindir)/scan

# install misc support binaries
install-misc:
	mkdir -p $(DESTDIR)$(libdir)
	for misc in $(MISC); do \
	  $(INSTALL_PROGRAM) $$misc $(DESTDIR)$(libdir)/$$misc; \
	done

# install commands with special installation needs (thus no $(SCMDS) use here)
install-scmds:
	if test x$(SETGID_MAIL) != x; then \
	  $(INSTALL_PROGRAM) -g $(MAIL_SPOOL_GRP) -m 2755 inc $(DESTDIR)$(bindir)/$$cmd; \
	else \
	  $(INSTALL_PROGRAM)                 inc $(DESTDIR)$(bindir)/$$cmd; \
	fi

uninstall:
	for cmd in $(CMDS); do \
	  rm -f $(DESTDIR)$(bindir)/$$cmd; \
	done
	for lcmd in $(LCMDS); do \
	  rm -f $(DESTDIR)$(bindir)/$$lcmd; \
	done
	for misc in $(MISC); do \
	  rm -f $(DESTDIR)$(libdir)/$$misc; \
	done
	for cmd in $(SCMDS); do \
	  rm -f $(DESTDIR)$(bindir)/$$cmd; \
	done

# ========== DEPENDENCIES FOR CLEANUP ==========

mostlyclean:
	rm -f *.o *~

clean: mostlyclean
	rm -f $(CMDS) $(MISC) $(SCMDS)

distclean: clean
	rm -f Makefile

realclean: distclean

superclean: realclean

# ========== DEPENDENCIES FOR LINT  ================

lint:
	$(LINT) $(LINTFLAGS) $(INCLUDES) $(DEFS) $(SRCS)

# ========== DEPENDENCIES FOR MAINTENANCE ==========

subdir = uip

Makefile: Makefile.in ../config.status
	cd .. && ./config.status $(subdir)/$@

